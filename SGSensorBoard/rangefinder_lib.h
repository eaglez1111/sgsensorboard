// SETUP

#include <Wire.h>
#include <SparkFun_VL6180X.h>

#define VL6180X_ADDRESS 0x29
#define PIN_LIGHTING 13 //need to change to 12 later

VL6180xIdentification identification;
VL6180x ToFProc(VL6180X_ADDRESS);

void setup_rangefinder(){
  
  Wire.begin(); //Start I2C library
  
  ToFProc.getIdentification(&identification); // Retrieve manufacture info from device memory
  if(ToFProc.VL6180xInit() != 0){
    Serial.println("FAILED TO INITALIZE");
  }
  
  ToFProc.VL6180xDefautSettings(); //Load default settings to get started.
  
}

byte get_rangefinder(int index){
  return (byte) ToFProc.getDistance();
}

void printIdentification(struct VL6180xIdentification *temp){
  Serial.print("Model ID = ");
  Serial.println(temp->idModel);

  Serial.print("Model Rev = ");
  Serial.print(temp->idModelRevMajor);
  Serial.print(".");
  Serial.println(temp->idModelRevMinor);

  Serial.print("Module Rev = ");
  Serial.print(temp->idModuleRevMajor);
  Serial.print(".");
  Serial.println(temp->idModuleRevMinor);  

  Serial.print("Manufacture Date = ");
  Serial.print((temp->idDate >> 3) & 0x001F);
  Serial.print("/");
  Serial.print((temp->idDate >> 8) & 0x000F);
  Serial.print("/1");
  Serial.print((temp->idDate >> 12) & 0x000F);
  Serial.print(" Phase: ");
  Serial.println(temp->idDate & 0x0007);

  Serial.print("Manufacture Time (s)= ");
  Serial.println(temp->idTime * 2);
  Serial.println();
}