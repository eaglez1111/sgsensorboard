
////////////////  GENERAL SETUP  //////////////////////////

# include <Arduino.h>
#include "rangefinder_lib.h"

#define PIN_ANALOG0 A0

////////////////  ROS SETUP  //////////////////////////

#include <ros.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <rosserial_arduino/Adc.h>

ros::NodeHandle nh;

////////////////  Initialize Publishers  //////////////////////////

rosserial_arduino::Adc adc_msg;
ros::Publisher p("rangefinder", &adc_msg);

/////////////////  Initialize Subscriber  //////////////////////////
void messageCb(const std_msgs::Bool& lighting){
  digitalWrite(PIN_LIGHTING, lighting.data);
}
ros::Subscriber<std_msgs::Bool> sub("front_lighting", &messageCb );
////////////////////////////////////////////////////////////////////

void setup()
{
  setup_rangefinder();
  
  delay(100);
  
  // ROS STUFF
  nh.initNode();
  nh.advertise(p);
  nh.subscribe(sub);
  // END ROS STUFF
}

void loop()
{
  adc_msg.adc0 = get_rangefinder(0);
  adc_msg.adc1 = analogRead(A0)/4;
  adc_msg.adc2 = analogRead(A1)/4;
  adc_msg.adc3 = analogRead(A2)/4;
  adc_msg.adc4 = analogRead(A3)/4;
  adc_msg.adc5 = analogRead(A4)/4;

  p.publish(&adc_msg);
  
  nh.spinOnce();
}